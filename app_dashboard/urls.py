from django.urls import path
from .views import (
    index,
    logging_in,
    logging_out,
    add_comment
)

urlpatterns = [
    path('', index, name='index'),
    path('add_comment/<int:post_id>/', add_comment, name='add_comment'),
    path('logging-in/', logging_in, name='logging-in'),
    path('logging-out/', logging_out, name='logging-out')
]
