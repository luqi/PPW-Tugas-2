from django.shortcuts import render
from django.http import HttpResponseRedirect

from app_company.models import Company
from app_forum.models import Post, Comment


def index(request):
    response = {}
    response['posts'] = Post.objects.all()
    if 'is_login' in request.session and "company_id" in request.session:
        company = Company.objects.filter(
            company_id=request.session['company_id']).first()
        response['company'] = company
    return render(request, 'dashboard.html', response)


def add_comment(request, post_id):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif "company_id" not in request.session:
        return render(request, 'pilih_company.html', {})
    elif request.method == 'POST':
        post = Post.objects.filter(pk=post_id).first()
        comment_input = request.POST['comment_input']

        if post is None or len(comment_input) == 0:
            return HttpResponseRedirect('/')

        Comment.objects.create(
            company=None,
            post=post,
            value=comment_input
        )
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')


def logging_in(request):
    if request.method == 'POST':
        request.session['is_login'] = True
        return HttpResponseRedirect('/select-company/')


def logging_out(request):
    if request.method == 'POST':
        request.session.flush()
        return HttpResponseRedirect('/')
