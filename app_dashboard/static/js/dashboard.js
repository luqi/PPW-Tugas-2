window.onload = () => {
  if(IN.User.isAuthorized()){
      $('#main-button').html(
        '<button class="btn btn-primary" type="button" onclick="LinkedInLogout()">Logout</button>'
      );
    }
    else {
      $('#main-button').html(
        '<input type="image" src="https://s.financesonline.com/media/themes/apps/gfx/linkedinButton.png"' +
                     'name="button-login" class="login-button" id="button-login" onclick="LinkedInLogin()"/>'
      );
  }
}

const LinkedInLogin = () => {
  IN.User.authorize(function(response){
    document.getElementById("login-form").submit();
  },{ scope:'r_basicprofile, r_emailaddress, rw_company_admin, w_share' });
};

const LinkedInLogout = () => {
  IN.User.logout(() => {
    document.getElementById("logout-form").submit();
  });
};