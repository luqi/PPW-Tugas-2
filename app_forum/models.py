from django.db import models

from app_company.models import Company


class Post(models.Model):
    company = models.ForeignKey(
        Company, related_name='posts', on_delete=models.CASCADE)
    value = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return str(self.value) + " | " + str(self.company)


class Comment(models.Model):
    company = models.ForeignKey(
        Company, related_name='comments', on_delete=models.CASCADE, null=True)
    post = models.ForeignKey(
        Post, related_name='comments', on_delete=models.CASCADE)
    value = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']

    def __str__(self):
        return str(self.value) + " | " + str(self.company)
