from django.urls import path

from app_company.views import (
    profile,
    store_company,
    select_company,
    forum,
    post_to_forum,
    delete_from_forum,
    add_company,
)


urlpatterns = [
    path('profile/', profile, name='profile'),
    path('select-company/', select_company, name='select_company'),
    path('store-company/', store_company, name='store_company'),
    path('add_company/', add_company, name='add_company'),
    path('forum/', forum, name='forum'),
    path('forum/post/', post_to_forum, name='post_to_forum'),
    path('forum/delete/', delete_from_forum, name='delete_from_forum')
]
