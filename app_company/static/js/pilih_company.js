window.onload = () => {
	if(IN.User.isAuthorized()){
		IN.API.Raw("/companies:(id,name,logo-url)?format=json&is-company-admin=true").method("GET").result(function(response) {
			if (response._total == 0){
				$('#select-company').html(
					"<h1> Kamu tidak punya perusahaan :( </h1>"
				);
			}
			else {
				$('#select-company').html('');
				html = '<div class="container">'
				response.values.map(company => {
					html += '<div class="row-flex well">';
					html += '<div class="img-company">';
					if('logoUrl' in company){
						html += '<img src="' + company.logoUrl + '">';
					} else {
						html += '<img src="https://oxpressprinting.files.wordpress.com/2015/08/apartment.png"' +
								'style=width=70px height=70px>'
					}
					html += '</div> <div>';
					html += '<button class="btn btn-link company-name" onclick="sendFormData(' + company.id + ')"><b>' +
							company.name + '</b></button>';
					html += '</div></div>';
					// $('#select-company').append(html);
				});
				html += '</div>'
				$('#select-company').append(html);
			}
		})
		.error(function(response){
			console.log(response)
		});
	}
};

const sendFormData = (company_id) => {
	document.getElementById("company_id_input").value = company_id;
	document.getElementById("store-company").submit();
}
