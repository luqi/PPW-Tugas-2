$(document).ready(function() {
    $('form[name=companyForm]').submit(function(){
	    $.post($(this).attr('action'), $(this).serialize(), function(res){
	     	return true;
	    });
	    return false;
	});
});

window.onload = () => {
	IN.API.Raw("/companies/"+company_id+":(id,name,logo-url,type,website-url,specialties,description)?format=json").method("GET").result(function(info) {
		html = '<div class="container content-all">'
		if('logoUrl' in info){
			html += '<div class="logo"><img src="'+info.logoUrl+'"></div>';
		}
		else {
			html += '<div class="logo"><img src="https://oxpressprinting.files.wordpress.com/2015/08/apartment.png"></div>';
		}
		html += '<div class="info">';
		html += '<h3 class="name">Name : '+info.name+'</h3>';
		html += '<h3 class="type">Type : '+info.type+'</h3>';
		if ('specialties' in info) {
			html += '<div class="specialties"><h3>Specialties : </h3>'
			info.specialties.values.map(specialty => {
				html += '<p class="specialty">'+specialty+'</p>'
			});
		}
		$('#profile-company').html(html);

		specialties = ''
		if(info.specialties){
			for(special in info.specialties.values){
				specialties += info.specialties.values[special] + ' | ';
			}
		}

		if(info.name) document.getElementById("input_name").value = info.name;
		if(info.logoUrl)document.getElementById("input_logo").value = info.logoUrl;
		if(info.type)document.getElementById("input_type").value = info.type;
		if(info.id) document.getElementById("input_id").value = info.id;
		document.getElementById("input_spec").value = specialties;

		$("#add-company").submit();
	});
};