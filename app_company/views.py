from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages

from app_company.models import Company
from app_forum.models import Post


def profile(request):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif "company_id" not in request.session:
        return HttpResponseRedirect('/select-company/')
    else:
        company = Company.objects.filter(
            company_id=request.session['company_id']).first()
        return render(request, 'profile.html',
                      {"company_id": request.session["company_id"]},
                      {'company': company})

def select_company(request):
    return render(request, 'pilih_company.html', {})


def add_company(request):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif "company_id" not in request.session:
        return HttpResponseRedirect('/select-company/')
    elif request.method == 'POST':
        company_id = request.POST.get('company_id')
        hitung = Company.objects.filter(company_id=company_id).count()
        if hitung == 0:
            Company.objects.create(
                name=request.POST.get('company_name'),
                type=request.POST.get('company_type'),
                image=request.POST.get('company_logo'),
                company_id=request.POST.get('company_id'),
                speciality=request.POST.get('company_spec'),
            )
        return HttpResponseRedirect('/profile/')
    else:
        return HttpResponseRedirect('/profile/')


def store_company(request):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif request.method == 'POST':
        request.session["company_id"] = request.POST.get('company_id')
        return HttpResponseRedirect('/profile/')
    else:
        return HttpResponseRedirect('/profile/')


def post_to_forum(request):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif "company_id" not in request.session:
        return render(request, 'pilih_company.html', {})
    elif request.method == 'POST':
        post_input = request.POST['post_input']
        company_id = request.session['company_id']
        company = Company.objects.filter(company_id=company_id).first()

        Post.objects.create(
            company=company,
            value=post_input
        )

        messages.add_message(
            request, messages.INFO, 'Post added..!!!',
            extra_tags='alert alert-info'
        )
        return HttpResponseRedirect('/forum/')
    else:
        return HttpResponseRedirect('/forum/')


def delete_from_forum(request):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif "company_id" not in request.session:
        return render(request, 'pilih_company.html', {})
    elif request.method == 'POST':
        post_id = request.POST['post_id']
        company_id = request.session['company_id']
        company = Company.objects.filter(company_id=company_id).first()

        post = Post.objects.filter(
            id=post_id,
            company=company,
        )
        post.delete()

        messages.add_message(
            request, messages.INFO, 'Post deleted..!!!',
            extra_tags='alert alert-danger'
        )

        return HttpResponseRedirect('/forum/')
    else:
        return HttpResponseRedirect('/forum/')


def forum(request):
    if 'is_login' not in request.session:
        return HttpResponseRedirect('/')
    elif "company_id" not in request.session:
        return HttpResponseRedirect('/select-company/')
    else:
        company_id = request.session['company_id']
        company = Company.objects.filter(company_id=company_id)
        if company is None:
            pass

        company = company.first()
        posts = Post.objects.filter(company=company)

        response = {
            'company': {
                'name': company.name,
                'image': company.image,
            },
            'posts': posts
        }
        return render(request, 'forum.html', response)
