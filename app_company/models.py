from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=128)
    type = models.CharField(max_length=64)
    speciality = models.CharField(max_length=64)

    company_id = models.CharField(max_length=16, default=0)
    image = models.URLField(default="")

    class Meta:
        ordering = ['-name']

    def __str__(self):
        return str(self.name) + " | " + str(self.speciality)
